package begear.bioritmi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BioritmiSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(BioritmiSpringApplication.class, args);
	}

}
